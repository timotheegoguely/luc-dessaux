# Luc Dessaux

## Design

[Design Guidelines & Resources](/.design/DESIGN.md)

## License

[CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
