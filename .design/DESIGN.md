# Luc Dessaux Design Guidelines & Resources

## Design relevant materials

Here is a list of design relevant information and materials:

### Fonts

Arial everywhere, or Arial Nova in Microsoft Office.

### Colors

- `#1A4157` Capital Blue
- `#0F8291` Blue Lagoon 
- `#E8F4F7` Milky Way 
- `#BE5932` Copper Red
- `#CC9F89` Light Copper 
- `#757575` Concrete 
- `#338075` Copper Verdigris 
- `#9DB4AB` Copper Patina 
- `#0C090A` Night

### Logos

You can find the set of logos in the `[assets/logo](../../assets/logo/)` folder.

### HTML signature

The HTML signature is available here:
`[signature.html](../../signature.html)`.

### Design Files

The Figma file with all branding guidelines can be viewed here:
[https://www.figma.com/file/N4TePmzx7yRNBiJUbfROB3/Luc-Dessaux?node-id=107%3A625&t=MMptkvQsyLAYk6iy-1](https://www.figma.com/file/N4TePmzx7yRNBiJUbfROB3/Luc-Dessaux?node-id=107%3A625&t=MMptkvQsyLAYk6iy-1)

## License

All design work is licensed under the [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
